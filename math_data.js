const readline = require("readline");
const rl       = readline.createInterface({
        input  : process.stdin,
        output : process.stdout
});

rl.question("Pick a number: ", (answer) => {
        d = 2 * answer
        s = answer * answer
        console.log("Double the number:", d)
        console.log("Square of the number:", s)
        rl.close();
});
